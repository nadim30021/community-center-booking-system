var express = require('express');
var router = express.Router();


var bodyParser = require('body-parser');

router.use(bodyParser.urlencoded({extended: false}));


var db = require('./db');

router.get('/', function(request, response){
	if(request.session.privi=="user"){
		response.render('view_home',{mysession:request.session.privi, username:request.session.username});
	}
	else
	{
		response.redirect('login');
	}
});

router.post('/', function(request, response){
	if(request.session.privi=="user"){
	date1= request.body.datepicker1;

	var t=0;

	var q = "SELECT event_date FROM booking";
	db.getData(q, null, function(result){

		for(var i=0;i<result.length;i++)
		{
			if(result[i].event_date==date1)
			{
				t=1;
				var data={'mysession':request.session.privi,'em':request.session.em}
				response.render('view_alreadybooked_afterLogin',data);

			}

		}
		if(t==0){
			var data={'selectedDate':date1,'mysession':request.session.privi,'em':request.session.em};
			response.render('view_canApply_afterLogin',data);
	}
	
	});
}
else{response.render('view_login');}

});


module.exports = router;