var express = require('express');
var socketIo = require('socket.io');
var router = express.Router();

var db = require('./db');

router.get('/', function(request, response){
	if(request.session.privi == "admin"){
		response.render('view_admin_home',{mysession:request.session.privi});
	}
	else
	{
		response.redirect('login');
	}
});

module.exports = router;