-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 01, 2017 at 07:53 AM
-- Server version: 5.5.36
-- PHP Version: 5.4.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `community_center_booking`
--

-- --------------------------------------------------------

--
-- Table structure for table `booking`
--

CREATE TABLE IF NOT EXISTS `booking` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `booking_date` varchar(15) NOT NULL,
  `event_date` varchar(15) NOT NULL,
  `event_type` varchar(15) NOT NULL,
  `user_email` varchar(50) NOT NULL,
  `food_list` varchar(100) NOT NULL,
  `first_payment` int(5) NOT NULL,
  `due_payment` int(5) NOT NULL,
  `status` varchar(10) NOT NULL DEFAULT 'running',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `booking`
--

INSERT INTO `booking` (`id`, `booking_date`, `event_date`, `event_type`, `user_email`, `food_list`, `first_payment`, `due_payment`, `status`) VALUES
(1, '04/22/2017', '04/26/2017', 'Marrige', 'dfsddfs@gmail.c', 'Polao,Chicken.', 5000, 15000, 'running'),
(2, '04/23/2017', '04/27/2017', 'Marrige', 'dfsddfs@gmail.c', 'huuhguh', 5000, 15000, 'running'),
(3, '04/22/2017', '04/23/2017', 'Marrige', 'dfsddfs@gmail.c', 'jgfh', 5000, 15000, 'running');

-- --------------------------------------------------------

--
-- Table structure for table `cancelation`
--

CREATE TABLE IF NOT EXISTS `cancelation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `event_id` int(11) NOT NULL,
  `user_email` varchar(50) NOT NULL,
  `reasone_of_cancelation` varchar(100) NOT NULL,
  `status` varchar(15) NOT NULL DEFAULT 'requested',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

-- --------------------------------------------------------

--
-- Table structure for table `user_info`
--

CREATE TABLE IF NOT EXISTS `user_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `email` varchar(60) NOT NULL,
  `phone` varchar(15) NOT NULL,
  `password` varchar(20) NOT NULL,
  `privilige` int(11) NOT NULL DEFAULT '111',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `user_info`
--

INSERT INTO `user_info` (`id`, `name`, `email`, `phone`, `password`, `privilige`) VALUES
(7, 'dsfsf', 'dfsddfs@gmail.c', '6464545', '111', 111),
(8, 'admin', 'admin@gmail.com', '1122334455', 'admin', 957);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
