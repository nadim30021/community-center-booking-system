var express = require('express');
var router = express.Router();
var expressValidator = require('express-validator');
var bodyParser = require('body-parser');

router.use(expressValidator());
router.use(bodyParser.urlencoded({extended: false}));

var db = require('./db');

router.get('/', function(request, response){
	if(request.session.privi=="user"){
		response.redirect('home');
	}

	if(request.session.privi=="admin"){
		response.redirect('adminHome');
	}
	response.render('view_registration',{errors: null});
});

router.post('/', function(request, response){

	request.check('name', 'Name is required').notEmpty();
	request.check('email', 'Email is required').notEmpty();
	request.check('email', 'Not a valid email').isEmail();;
	request.check('phone', 'Phone number is required').notEmpty();
	request.check('password', 'Password is required').notEmpty();
	request.check('confirm', 'Password do not match').equals(request.body.password);	
	
	var problems = request.validationErrors();

	if(problems)
	{
		response.render('view_registration', {errors: problems});
	}
	else{
		name= request.body.name;
		email= request.body.email;
		phone= request.body.phone;
		password= request.body.password;

		var q = "INSERT INTO user_info(name,email,phone,password) VALUES('"+name+"','"+email+"','"+phone+"','"+password+"')";
		db.getData(q, null, function(result){
			response.redirect('/login');
		});
	}	
});


module.exports = router;