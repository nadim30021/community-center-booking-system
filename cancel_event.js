
var express = require('express');
var session = require('express-session');
var router = express.Router();

var bodyParser = require('body-parser');

router.use(bodyParser.urlencoded({extended: false}));

var db = require('./db');

router.get('/', function(request, response){
	if(request.session.privi=="user"){
		var em = request.session.em;
		var q = "SELECT * FROM booking where user_email='"+em+"' and status='running'";
		db.getData(q, null, function(result){
			var data = {
				'event': result 
			};
			response.render('view_cancel_event', data);
		});	
	}else{
		response.redirect('login');
	}
});

router.get('/:id', function(request, response){
	if(request.session.privi=="user"){
	var eid = request.params.id;
	var q = "SELECT * FROM booking where id=" + eid ;
	db.getData(q, null, function(result){
		var data = {'event': result};
		response.render('confirm_user_cancel_view', data);
	});	
	}else{
		response.redirect('login');
	}
});

router.post('/', function(request, response){

	if(request.session.privi=="user"){
		var em = request.session.em;
		var eventID = request.body.eventID;
		var useremail = request.body.useremail;
		var reasonOfCancelling = request.body.reasonOfCancelling;

		var u = "UPDATE booking SET status='requested' where id="+eventID;
		
		db.getData(u, null, function(result){});

		var i = "Insert into cancelation(event_id,user_email,reasone_of_cancelation,status) values("+eventID+",'"+useremail+"','"+reasonOfCancelling+"','requested')";

		db.getData(i, null, function(result){});
		var q = "SELECT * FROM booking where user_email='"+useremail+"' and status='running'";

		db.getData(q, null, function(result){
			var data = {'event': result};
			response.render('view_cancel_event',data);
		});
	}else{
		response.render('view_login');
	}
});

module.exports = router;