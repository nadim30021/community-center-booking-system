var express = require('express');
var socketIo = require('socket.io');
var expressSession = require('express-session');
var app = express();

var index= require('./index');
var login= require('./login');
var registration= require('./registration');
var home= require('./home');
var admin_home = require('./adminHome');
var cancle_request = require('./manage_cancel_request');
var event_list = require('./upcomiming_event_list');
var manage_due = require('./manage_due_payment');
var show_report = require('./show_report');
var apply_for_event = require('./apply_for_event');
var cancel_event = require('./cancel_event');
var my_events = require('./my_events');
var confirm_apply_for_event = require('./confirm_apply_for_event');
var logout = require('./logout');


app.set('view engine', 'ejs');

app.use(expressSession({
  secret: 'top secret',
  resave: false,
  saveUninitialized: true
}));

app.use('/', index);
app.use('/index', index);
app.use('/login', login);
app.use('/registration', registration);
app.use('/home', home);
app.use('/adminHome', admin_home);
app.use('/manage_cancel_request', cancle_request);
app.use('/upcomiming_event_list', event_list);
app.use('/manage_due_payment', manage_due);
app.use('/show_report', show_report );
app.use('/apply_for_event',apply_for_event );
app.use('/cancel_event', cancel_event);
app.use('/my_events', my_events);
app.use('/confirm_apply_for_event', confirm_apply_for_event);
app.use('/logout', logout);


app.use(express.static('./style'));

var server =app.listen(1338, function(i){
	console.log('server started at @1338 ...');
});

