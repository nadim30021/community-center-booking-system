var express = require('express');
var session = require('express-session');
var router = express.Router();

var bodyParser = require('body-parser');

router.use(bodyParser.urlencoded({extended: false}));
router.use(session({secret: 'ssshhhhh'}));

var sess;

var db = require('./db');
router.get('/:id', function(request, response){

	if(request.session.privi=="admin"){
		var id = request.params.id;
		var q = "SELECT * FROM booking where id= "+id;
		db.getData(q, null, function(result){
			var data = {'event': result};
			response.render('view_admin_manage_due_payment', data);
		});
	}else{
		response.redirect('login');
	}	
});


router.post('/', function(request, response){
	if(request.session.privi=="admin"){
		 event_id= request.body.event_id;
		 first_payment= request.body.first_payment;
		 due_payment= request.body.due_payment;
		 payment = parseInt(first_payment) + parseInt(due_payment);

		var q = "UPDATE booking SET first_payment = "+payment+", due_payment = 0 , status = 'clear' WHERE id="+event_id;
		db.getData(q, null, function(result){
			 response.redirect('upcomiming_event_list');
		});
	}else{
		response.render('view_login');
	}
});


module.exports = router;