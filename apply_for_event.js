var express = require('express');
var socketIo = require('socket.io');
var router = express.Router();

var bodyParser = require('body-parser');

router.use(bodyParser.urlencoded({extended: false}));



var db = require('./db');
router.get('/', function(request, response){
	if(request.session.privi=="user"){
		response.render('view_apply_for_event');
	}
	else{
		response.redirect('login');
	}
});


router.post('/', function(request, response){

	if(request.session.privi=="user"){
		var selectedDate = request.body.date;
		var useremail = request.body.user_email;

		//console.log(selectedDate+'   ' +useremail);
		var data = {'selectedDate': selectedDate, 'useremail':useremail};
		response.render('view_apply_for_event',data );
	}
	else
	{
		response.redirect('login');
	}

});

module.exports = router;