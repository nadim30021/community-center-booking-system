
var express = require('express');
var session = require('express-session');
var router = express.Router();

var bodyParser = require('body-parser');

router.use(bodyParser.urlencoded({extended: false}));

var db = require('./db');

router.get('/', function(request, response){
	if(request.session.privi=="user"){
		var em = request.session.em
		var q = "SELECT * FROM booking where user_email='"+em+"'";
		db.getData(q, null, function(result){
			var data = {'event': result};
			response.render('view_my_events', data);
		});	
	}
	else
	{
		response.redirect('login');
	}
});

module.exports = router;