var express = require('express');
var router = express.Router();

var bodyParser = require('body-parser');

router.use(bodyParser.urlencoded({extended: false}));

var db = require('./db');
router.get('/', function(request, response){
	if(request.session.privi=="admin")
		response.render('view_admin_show_report');
	else
		response.redirect('login');	
});

router.post('/', function(request, response){
	
	date1= request.body.datepicker1;
	date2= request.body.datepicker2;
	
	var q = "SELECT * FROM booking where event_date>='"+date1+"' and event_date<='"+date2+"'";
	db.getData(q, null, function(result){
		var data = {'event': result};
		response.render('view_admin_report', data);
	
	});
});

module.exports = router;