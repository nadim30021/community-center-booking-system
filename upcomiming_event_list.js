var express = require('express');
var router = express.Router();


var bodyParser = require('body-parser');

router.use(bodyParser.urlencoded({extended: false}));

var db = require('./db');

router.get('/', function(request, response){	
	if(request.session.privi=="admin"){
	var q = "SELECT * FROM booking where status='running' order by event_date";
	db.getData(q, null, function(result){
		var data = {'event': result,'mysession':request.session.privi};

		response.render('view_admin_upcoming_event', data);
	});
	}
	else{
		response.redirect('login');
	}
});


module.exports = router;