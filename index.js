var express = require('express');
var router = express.Router();

var bodyParser = require('body-parser');

router.use(bodyParser.urlencoded({extended: false}));

var db = require('./db');
router.get('/', function(request, response){
	if(request.session.privi=="user"){
		response.redirect('home');
	}

	if(request.session.privi=="admin"){
		response.redirect('adminHome');
	}
	response.render('view_index');
});


router.post('/', function(request, response){
	
	date1= request.body.datepicker1;
	var t=0;

	var q = "SELECT event_date FROM booking";
	db.getData(q, null, function(result){
		for(var i=0;i<result.length;i++)
		{
			if(result[i].event_date==date1)
			{
				t=1;
				response.render('view_alreadybooked');
			}
		}
		if(t==0){
		response.render('view_canApply');}
	});
});
	
module.exports = router;