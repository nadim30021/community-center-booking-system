var express = require('express');
var session = require('express-session');
var router = express.Router();

var bodyParser = require('body-parser');

router.use(bodyParser.urlencoded({extended: false}));
router.use(session({secret: 'ssshhhhh'}));

var db = require('./db');
router.get('/', function(request, response){
	if(request.session.privi=="admin"){
		var q = "SELECT * FROM booking,cancelation where booking.id=cancelation.event_id and cancelation.status='requested' order by event_date";
		db.getData(q, null, function(result){
			var data = {'cancel': result};
			response.render('view_admin_manage_cancel_request', data);
		});
	}else{
		response.render('view_login');
	}	
});

router.get('/:id', function(request, response){

	if(request.session.privi=="admin"){
		var ide = request.params.id;
		var q = "SELECT * FROM booking,cancelation where booking.id=cancelation.event_id and cancelation.status='requested' and cancelation.id="+ide;
		db.getData(q, null, function(result){
			var data = {'cancel': result};
			response.render('view_admin_cancel_request_process', data);
		});
	}else{
		response.render('view_login');
	}
});


router.post('/', function(request, response){

	if(request.session.privi=="admin"){
		cancel_id = request.body.cancel_id;
		event_id = request.body.event_id;
		status = request.body.status;

		var q = "UPDATE cancelation SET status = 'Canceled' WHERE id= "+cancel_id;
		db.getData(q, null, function(result){});

		var p = "UPDATE booking SET  due_payment = 0 ,status = 'Canceled'  WHERE id= "+event_id;
		db.getData(p, null, function(result){
			response.redirect('manage_cancel_request');
		});
	}
	else{
		response.render('view_login');
	}
});

module.exports = router;