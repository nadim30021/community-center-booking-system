var express = require('express');
var socketIo = require('socket.io');
var router = express.Router();
var db = require('./db');
var bodyParser = require('body-parser');

router.use(bodyParser.urlencoded({extended: false}));


router.post('/', function(request, response){

	if(request.session.privi=="user"){

		var selectedDate = request.body.selectedDate;
		var useremail = request.body.useremail;
		var event_type = request.body.event_type;
		var people = request.body.people;
		var foods = request.body.foods;

		var total_amount = request.body.amount1;
		var first_payment = (total_amount * 15)/100;
		var due_payment = total_amount - first_payment;

		console.log(total_amount);

		var dateObj = new Date();
		var month = dateObj.getUTCMonth() + 1;
		var day = dateObj.getUTCDate();
		var year = dateObj.getUTCFullYear();

		booking_date = month + "/" + day + "/" + year;

		var q = "INSERT INTO booking VALUES(null,'"+booking_date+"','"+selectedDate+"','"+event_type+"','"+useremail+"','"+foods+"','"+first_payment+"','"+due_payment+"','running')";
		db.getData(q, null, function(result){
			response.redirect('home');
		});
	}
	else{
		response.redirect('login');
	}

});

module.exports = router;